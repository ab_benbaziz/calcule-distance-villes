FROM openjdk:8
LABEL maintainer ="ab_benbaziz@esi.dz"
VOLUME /tmp
EXPOSE 8086
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} calcule-distance-ville-api.jar

ENTRYPOINT ["java","-jar","/calcule-distance-ville-api.jar"]
