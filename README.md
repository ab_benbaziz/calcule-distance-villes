# calcule-distance-villes



## Getting started - Clone the project

https://gitlab.com/ab_benbaziz/calcule-distance-villes.git


## Docker Compose to run API on Docker

cmd: docker compose up

## END POINTS

* GET ALL CITIES [path = "/all"]:    http://localhost:8080/api/v1/villes/all

* GET CITY BY ID [path = "{villeId}"]:    

    http://localhost:8080/api/v1/villes/1
        Resultat:
                {
                "pays": "Algérie",
                "id": 1,
                "nom": "Alger",
                "Code Postal": "16000",
                "latitude": 36.752887,
                "longitude": 3.042048
                }
                
    http://localhost:8080/api/v1/villes/2
        Resultat:  
                 {
                "pays": "Algérie",
                "id": 2,
                "nom": "Annaba",
                "Code Postal": "23000",
                "latitude": 36.8982165,
                "longitude": 7.7549272
                 }
        
* ADD A CITY [path = "/add"]:

    http://localhost:8080/api/v1/villes/add

* EDIT A CITY [path = "/Edit"]:

    http://localhost:8080/api/v1/villes/Edit

* DELETE A CITY [path = "/Delete/{id_ville}"]:

    http://localhost:8080/api/v1/villes/Delete/2

* CALCULE DISTANCE [path = "/calcule-distance/{id_ville_1}/{id_ville_2}"]:

    http://localhost:8080/api/v1/villes/calcule-distance/1/2

    Resultat : 
        La Distance entre Alger et Annaba est de : 419.75 KM.






