package com.dev.calculedistanceville.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VilleDTO implements Serializable {

    @JsonProperty("id")
    private long id;

    @NotNull
    @JsonProperty("nom")
    private String nom;

    @JsonProperty("Code Postal")
    private String codePostal;

    private String pays;

    @NotNull
    @JsonProperty("latitude")
    private double latitude;

    @NotNull
    @JsonProperty("longitude")
    private double longitude;

}
