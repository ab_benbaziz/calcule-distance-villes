package com.dev.calculedistanceville.controller;

import com.dev.calculedistanceville.DTO.VilleDTO;
import com.dev.calculedistanceville.service.VilleService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.DecimalFormat;
import java.util.List;


/**
 * The VilleController Class
 * Handiling HTTP requests
 *
 * @author BADREDDINE BENBAZIZ
 * @version 1.0
 * Date 29/07/2022.
 */

@RestController
@RequestMapping("/api/v1/villes")
@AllArgsConstructor
public class VilleController {


    // Le service VilleService

    private static final DecimalFormat df = new DecimalFormat("0.00");
    private final VilleService villeService;

    @GetMapping(path = "{villeId}")
    public ResponseEntity<VilleDTO> getVille(@PathVariable("villeId") Integer villeId) {

        if (villeService.findById(villeId).isPresent())
            // si la ville existe dans la base de données
            return new ResponseEntity<>(villeService.findById(villeId).get(), HttpStatus.OK);

        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<VilleDTO>> getAllVilles() {

        List<VilleDTO> villeDTOList = villeService.findAll();
        if (villeDTOList.size() > 0)
            return new ResponseEntity<List<VilleDTO>>(villeService.findAll(), HttpStatus.OK);
        else return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/calcule-distance/{id_ville_1}/{id_ville_2}")
    public String calculeDistance(@PathVariable("id_ville_1") long id_ville_1,
                                  @PathVariable("id_ville_2") long id_ville_2) {
        VilleDTO ville1, ville2;
        double distance;


        if (villeService.findById(id_ville_1).isPresent()) {
            ville1 = villeService.findById(id_ville_1).get();
            System.out.println("ville1.getLatitude() = " + ville1.getLatitude());
            if (villeService.findById(id_ville_2).isPresent()) {
                ville2 = villeService.findById(id_ville_2).get();
                System.out.println("ville2.getLatitude() = " + ville2.getLatitude());
                distance = villeService.calculeDistance(ville1, ville2);
            } else {
                return "Ville 2 introuvable !";
            }
        } else return "Ville 1 inrouvable !";
        return "La Distance entre " + ville1.getNom() + " et " + ville2.getNom() + " est de : " + df.format(distance) + " KM.";
    }

    @PostMapping(path = "/add")
    @ResponseBody
    public String ajouterVille(@RequestBody VilleDTO villeDTO) {
        VilleDTO newVilleDTO = VilleDTO.builder()
                .nom(villeDTO.getNom())
                .pays(villeDTO.getPays())
                .codePostal(villeDTO.getCodePostal())
                .latitude(villeDTO.getLatitude())
                .longitude(villeDTO.getLongitude())
                .build();

        return villeService.save(newVilleDTO).toString();
    }

    @PostMapping(path = "/Edit")
    @ResponseBody
    public String modifierVille(@RequestBody VilleDTO villeDTO) {
        VilleDTO newVilleDTO = VilleDTO.builder()
                .id(villeDTO.getId())
                .nom(villeDTO.getNom())
                .pays(villeDTO.getPays())
                .codePostal(villeDTO.getCodePostal())
                .latitude(villeDTO.getLatitude())
                .longitude(villeDTO.getLongitude())
                .build();

        return villeService.update(newVilleDTO).toString();
    }

    @DeleteMapping(path = "/Delete/{id_ville}")
    @ResponseBody
    public String supprimerVille(@PathVariable("id_ville") long id_ville) {

        if (villeService.delete(id_ville))
            return "Ville Supprimée !";
        else
            return "Ville introuvable !";
    }


}
