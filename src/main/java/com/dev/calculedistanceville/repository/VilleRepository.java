package com.dev.calculedistanceville.repository;

import com.dev.calculedistanceville.model.Ville;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VilleRepository extends JpaRepository<Ville, Long> {

}
