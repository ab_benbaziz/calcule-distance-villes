package com.dev.calculedistanceville.service;

import com.dev.calculedistanceville.model.Ville;
import com.dev.calculedistanceville.repository.VilleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class VilleServiceTest {

    @MockBean
    private VilleRepository villeRepository;


    @Test
    public void saveTest() {
        // Préparation des données
        Ville ville = Ville.builder()
                .nom("Alger")
                .pays("Algérie")
                .codePostal("16000")
                .latitude(36.752887)
                .longitude(3.042048)
                .build();
        //Action
        villeRepository.save(ville);
        //Assertion
        verify(villeRepository, times(1)).save(ville);

    }

    @Test
    public void deleteTest() {
        // Préparation des données
        Ville ville = Ville.builder()
                .id(10)
                .nom("Djidjel")
                .pays("Algérie")
                .codePostal("18000")
                .latitude(36.752887)
                .longitude(3.042048)
                .build();

        villeRepository.save(ville);
        //Action
        villeRepository.deleteById(10L);
        //Assertion
        verify(villeRepository, times(1)).deleteById(ville.getId());


    }


    /*Cette method retourne
            une ville si l'enregistrement existe, et
            null si l'enregistrement n'existe pas.
     */
    @Test
    public void findByIdTest() {

        Ville ville = Ville.builder()
                .id(10)
                .nom("Alger")
                .pays("Algérie")
                .codePostal("16000")
                .latitude(36.752887)
                .longitude(3.042048)
                .build();

        when(villeRepository.findById(ville.getId())).thenReturn(Optional.of(ville));
        //Action
        Ville v = villeRepository.findById(ville.getId()).get();
        Assertions.assertNotNull(v);
        Assertions.assertEquals(ville.getId(), v.getId());
        Assertions.assertEquals(ville.getNom(), v.getNom());
        Assertions.assertEquals(ville.getLatitude(), v.getLatitude());
        Assertions.assertEquals(ville.getLongitude(), v.getLongitude());
        Assertions.assertEquals(ville.getCodePostal(), v.getCodePostal());
    }


}
